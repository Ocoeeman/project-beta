# CarCar

Team:

* Person 1 - Alex Natavio - Service
* Person 2 - Daniel Flowers - Sales

## Design

## Service microservice

Inventory API was created providing the Models: Manufacturer, Automobile and Vehicle Model.  A poller was created and utilized to retrieve the VIN as the unique property that identifies the Automobile.

Backend:  Using Django microservices the user can register a Technician.  After registering a Technician, a Service Appointment can then be created and assigned with both a Technician and a VIN retrieved from the poller.

Frontend:  Using ReactJS, the user can see the list of Technicians, as well as register a new Technician.  The ability to create a new Service Appointment is also available.  A list of currently scheduled Service Appointments is available, with the ability to both Cancel or Finish the service which will remove the selected appointment from the currently scheduled list.  Lastly, a detailed service history list of all current and past Service Appointments is available with a search bar that allows the user to search for all appointments that belong to a specified VIN.

## Sales microservice

In sales I have 3 models: Salesperson, Customer, and Sale.
In addition to this I have a Value Object related
to the Automobile Model from Inventory.
I am intigrating these so that I will be able to
take a vehicle from beginning to sold, with
customer and salesperson data attached to the car.
