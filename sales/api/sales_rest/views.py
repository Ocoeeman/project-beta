from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import (
    SalespersonEncoder,
    CustomerEncoder,
    SaleEncoder,
)
from .models import Salesperson, Customer, Sale, AutomobileVO


@require_http_methods(["GET", "POST"])
def api_salesperson_list(request):
    if request.method == "GET":
        autos = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": autos},
            encoder=SalespersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            person = Salesperson.objects.create(**content)
            return JsonResponse(
                person,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the salesperson"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_salesperson_detail(request, id):
    if request.method == "GET":
        try:
            auto = Salesperson.objects.get(id=id)
            return JsonResponse(
                auto,
                encoder=SalespersonEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            auto = Salesperson.objects.get(id=id)
            auto.delete()
            return JsonResponse(
                auto,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            auto = Salesperson.objects.get(id=id)

            props = ["first_name", "last_name", "employee_id"]
            for prop in props:
                if prop in content:
                    setattr(auto, prop, content[prop])
            auto.save()
            return JsonResponse(
                auto,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_customer_list(request):
    if request.method == "GET":
        autos = Customer.objects.all()
        return JsonResponse(
            {"customers": autos},
            encoder=CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the customer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_customer_detail(request, id):
    if request.method == "GET":
        try:
            auto = Customer.objects.get(id=id)
            return JsonResponse(
                auto,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            auto = Customer.objects.get(id=id)
            auto.delete()
            return JsonResponse(
                auto,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            auto = Customer.objects.get(id=id)

            props = ["first_name", "last_name", "address", "phone_number"]
            for prop in props:
                if prop in content:
                    setattr(auto, prop, content[prop])
            auto.save()
            return JsonResponse(
                auto,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_sale_list(request):
    if request.method == "GET":
        autos = Sale.objects.all()
        return JsonResponse(
            {"sales": autos},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.get(employee_id=content["salesperson"])
            content["salesperson"] = salesperson

            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer

            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile

            sale = Sale.objects.create(**content)

            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )

        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Salesperson does not exist"})

        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist"})

        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message": "Automobile does not exist"})


@require_http_methods(["DELETE", "GET", "PUT"])
def api_sale_detail(request, id):
    if request.method == "GET":
        try:
            auto = Sale.objects.get(id=id)
            return JsonResponse(
                auto,
                encoder=SaleEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            auto = Sale.objects.get(id=id)
            auto.delete()
            return JsonResponse(
                auto,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            auto = Sale.objects.get(id=id)

            props = ["id", "vin", "sold"]
            for prop in props:
                if prop in content:
                    setattr(auto, prop, content[prop])
            auto.save()
            return JsonResponse(
                auto,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
