from django.urls import path
from .views import (
    api_salesperson_list,
    api_salesperson_detail,
    api_customer_list,
    api_customer_detail,
    api_sale_list,
    api_sale_detail,
)

urlpatterns = [
    path("salespeople/", api_salesperson_list, name="api_salesperson_list"),
    path("salespeople/<int:id>/", api_salesperson_detail, name="api_salesperson_detail"),
    path("customers/", api_customer_list, name="api_customer_list"),
    path("customers/<int:id>/", api_customer_detail, name="api_customer_detail"),
    path("sales/", api_sale_list, name="api_sale_list"),
    path("sales/<int:id>", api_sale_detail, name="api_sale_detail"),
]
