from django.urls import path
from service_rest.views import (api_list_technicians,
                                api_list_appointments,
                                api_detail_technician,
                                api_detail_appointments,
                                api_cancel_appointment,
                                api_finish_appointment,)


urlpatterns = [
    path('technicians/', api_list_technicians, name="api_list_technicians"),
    path('technicians/<int:id>/', api_detail_technician, name="api_detail_technician"),
    path('appointments/', api_list_appointments, name="api_list_appointments"),
    path('appointments/<int:id>/', api_detail_appointments, name="api_detail_appointments"),
    path('appointments/<int:id>/cancel/', api_cancel_appointment, name="api_cancel_appointment"),
    path('appointments/<int:id>/finish/', api_finish_appointment, name="api_finish_appointment")

]
