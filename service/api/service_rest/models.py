from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    first_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=25)
    employee_id = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return f"{self.last_name}, {self.first_name}"


class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    date_time = models.DateTimeField(null=True)
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="scheduled")
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.CASCADE
    )

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})

    def __str__(self):
        return f"{self.date_time} - {self.customer}"

    def cancel(self):
        self.status = "canceled"
        self.save()

    def finish(self):
        self.status = "finished"
        self.save()
