from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Technician, Appointment
from .encoders import TechnicianEncoder, AppointmentEncoder


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technicians": technician},
            encoder=TechnicianEncoder,
            safe=False
        )
    else:
        new_tech = json.loads(request.body)
        technician = Technician.objects.create(**new_tech)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )


@require_http_methods(["GET","DELETE"])
def api_detail_technician(request, id):
    if request.method == "GET":
        technician = Technician.objects.get(id=id)
        print("technician.id:", technician.id)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=id)
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"}, status=404
            )
        technician.delete()
        return JsonResponse(
            {"message": "Technician was deleted successfully"},
            status=200
        )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointment = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointment},
            encoder=AppointmentEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)

        try:
            technician = Technician.objects.get(employee_id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"},
                status=400
            )
        new_appointment = Appointment.objects.create(**content)
        return JsonResponse(
            new_appointment,
            encoder=AppointmentEncoder,
            safe=False
        )


@require_http_methods(["GET","DELETE"])
def api_detail_appointments(request, id):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=id)
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status=404
            )

        appointment.delete()
        return JsonResponse(
            {"message": "Appointment was deleted successfully"},
            status=200
        )


@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.cancel()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False
    )


@require_http_methods(["PUT"])
def api_finish_appointment(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.finish()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False
    )
