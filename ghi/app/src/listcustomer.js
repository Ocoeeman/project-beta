import React, {useEffect, useState} from "react";

function CustomerList() {
    const [customers, setCustomers] = useState([])

    async function loadCustomers() {
        const response = await fetch('http://localhost:8090/api/customers/')

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        }
    }

    useEffect(() => {
        loadCustomers();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Address</th>
                    <th>Phone Number</th>
                </tr>
            </thead>
            <tbody>
                {customers.map(salesperson => {
                    return (
                        <tr key={ salesperson.id }>
                            <td>{ salesperson.first_name }</td>
                            <td>{ salesperson.last_name }</td>
                            <td>{ salesperson.address }</td>
                            <td>{ salesperson.phone_number }</td>
                        </tr>)
                })}
            </tbody>
        </table>
    )
}

export default CustomerList
