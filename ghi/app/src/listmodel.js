import React, {useEffect, useState} from "react";

function ModelList() {
    const [models, setAutos] = useState([])

    async function loadAutos() {
        const response = await fetch('http://localhost:8100/api/models/')

        if (response.ok) {
            const data = await response.json();
            setAutos(data.models)
        }
    }

    useEffect(() => {
        loadAutos();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Picture</th>
                    <th>Manufacturer</th>
                </tr>
            </thead>
            <tbody>
                {models.map((auto) => (
                    <tr key={auto.name}>
                        <td>{auto.name}</td>
                        <td><img src={auto.picture_url}
                                 alt="car picture"
                                 style={{ maxWidth: '200px', maxHeight: '200px' }}/>
                        </td>
                        <td>{auto.manufacturer.name}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    )
}

export default ModelList
