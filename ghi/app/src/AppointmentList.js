import React, { useEffect, useState } from 'react';

function AppointmentList() {
  const [appointments, setAppointments] = useState([]);

  useEffect(() => {
    async function loadAppointments() {
        const appointmentResponse = await fetch('http://localhost:8080/api/appointments/');
        if (appointmentResponse.ok) {
          const appointmentData = await appointmentResponse.json();
          setAppointments(appointmentData.appointments);
        } else {
          console.log("Error fetching appointments data");
        }
      }
    loadAppointments();
  }, []);

  const handleCancelAppointment = async (appointmentId) => {
    const confirmCancel = window.confirm("Are you sure you want to cancel?");
    if (confirmCancel) {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${appointmentId}/cancel/`,
            {method: "PUT",
        headers: {
            'Content-Type': 'application/json',
        },
    });

    if (response.ok) {
        setAppointments(prevAppointments =>
           prevAppointments.map(appointment =>
            appointment.id === appointmentId ? { ...appointment, status: 'canceled'} : appointment)
            );
    } else {
        console.log("Failed to cancel appointment");
    }
    } catch (error) {
        console.log("Error when cancelling appointment", error);
    }
    }
  };

  const handleFinishAppointment = async (appointmentId) => {
    const confirmFinish = window.confirm("Is the appointment finished?");
    if (confirmFinish) {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${appointmentId}/finish/`,
            {method: "PUT",
        headers: {
            'Content-Type': 'application/json',
        },
    });

    if (response.ok) {
        setAppointments(prevAppointments =>
           prevAppointments.map(appointment =>
            appointment.id === appointmentId ? { ...appointment, status: 'finished'} : appointment)
            );
    } else {
        console.log("Failed to finish appointment");
    }
    } catch (error) {
        console.log("Error when finishing appointment", error);
    }
    }
  };

  return (
    <div className="px-4 py-5 my-5">
      <h1 className="display-5 fw-bold">Service Appointments</h1>
      <div className="mx-auto">
      <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Customer</th>
          <th>Date</th>
          <th>Time</th>
          <th>Technician</th>
          <th>Reason</th>
        </tr>
      </thead>
      <tbody>
        {appointments.map((appointment) => {
            const appointmentDate = new Date(appointment.date_time)
            const appointmentTime = new Date(appointment.date_time)

            if (appointment.status !== 'finished' && appointment.status !== 'canceled') {

          return (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{appointment.customer}</td>
              <td>{appointmentDate.toLocaleDateString()}</td>
              <td>{appointmentTime.toLocaleTimeString(undefined, { timeZone: 'UTC' })}</td>
              <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
              <td>{appointment.reason}</td>
              <td>
                    {appointment.status !== 'finished' && (
                      <button
                        onClick={() => handleFinishAppointment(appointment.id)}
                        className="btn btn-success"
                      >
                        Finish
                      </button>
                    )}
                    {appointment.status !== 'canceled' && (
                      <button
                        onClick={() => handleCancelAppointment(appointment.id)}
                        className="btn btn-danger mx-2"
                      >
                        Cancel
                      </button>
                    )}
                   </td>
                </tr>
              );
            } return null;
        })}
      </tbody>
    </table>

    </div>
    </div>
  );
}

export default AppointmentList;
