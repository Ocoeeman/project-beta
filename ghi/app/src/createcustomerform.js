import { useState, useEffect } from "react";

function CreateCustomerForm(){
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [address, setAddress] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");

    const handlefirstNameChange = (banana) => {
        setFirstName(banana.target.value)
    }

    const handlelastNameChange = (coconut) => {
        setLastName(coconut.target.value)
    }

    const handleaddressChange = (pineapple) => {
        setAddress(pineapple.target.value)
    }

    const handlephonenumberChange = (papaya) => {
        setPhoneNumber(papaya.target.value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.address = address;
        data.phone_number = phoneNumber;

        const salespersonUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok) {
            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('')

        } else {
            console.error('Error sending form')
        }
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1 className="card-title">Add a Customer</h1>
                <form onSubmit={handleSubmit} id="create-customer-form">
                <div className="col">
                    <div className="form-floating mb-3">
                    <input required onChange={handlefirstNameChange} value={firstName} placeholder="example Mickey" type="text" id="first_name" name="first_name" className="form-control" />
                    <label htmlFor="firstName">Customer First Name</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <input required onChange={handlelastNameChange} value={lastName} placeholder="example Mouse" type="text" id="last_name" name="last_name" className="form-control" />
                    <label htmlFor="lastName">Salesperson Last Name</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <input required onChange={handleaddressChange} value={address} placeholder="123 Mockingbird Ln" type="text" id="address" name="address" className="form-control" />
                    <label htmlFor="address">Address</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <input required onChange={handlephonenumberChange} value={phoneNumber} placeholder="example 1" type="text" id="employee_id" name="employee_id" className="form-control" />
                    <label htmlFor="phoneNumber">Phone Number</label>
                    </div>
                </div>
                <button className="btn btn-primary">Add Customer</button>
                </form>
            </div>
        </div>
        </div>
    )

}

export default CreateCustomerForm;
