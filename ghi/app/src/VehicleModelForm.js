import React, { useState, useEffect } from 'react';

function VehicleModelForm() {
  const [formData, setFormData] = useState({
    name: '',
    picture_url: '',
    manufacturer_id: '',
  });

  const [manufacturers, setManufacturers] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8100/api/models/';

    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        name: '',
        picture_url: '',
        manufacturer_id: '',
      });
      alert("Model added successfully!");
    } else {
      alert("Failed to add model.");
    }
  };

  const handleFormChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevFormData) => ({ ...prevFormData, [name]: value }));
  };
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a vehicle model</h1>
          <form onSubmit={handleSubmit} id="create-vehicle-model-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="employeeId">Model Name...</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Name" required type="text" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="employeeId">Picture URL...</label>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                <option value="">Choose a manufacturer...</option>
                {manufacturers.map(manufacturer => {
                  return (
                    <option key={manufacturer.id} value={manufacturer.id}>
                    {manufacturer.name}
                    </option>
                  )
                })}
              </select>
              <label htmlFor="manufacturer_id"></label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default VehicleModelForm;
