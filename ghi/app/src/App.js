import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespersonList from './listsalesperson';
import CreateSalespersonForm from './createsalespersonform';
import AutomobileList from './listautomobile';
import CustomerList from './listcustomer';
import CreateCustomerForm from './createcustomerform';
import SaleList from './listsales';
import CreateSalesForm from './createsaleform';
import ManufacturerList from './listmanufacturer';
import ModelList from './listmodel';
import CreateAutomobileForm from './createautomobileform';
import SaleHistoryList from './listsaleshistory';
import TechnicianList from './TechnicianList'
import TechnicianForm from './TechnicianForm'
import AppointmentForm from './AppointmentForm'
import AppointmentList from './AppointmentList'
import ServiceHistory from './ServiceHistory';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelForm from './VehicleModelForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/salesperson" element ={<SalespersonList />}/>
          <Route path="/salesperson/create" element ={<CreateSalespersonForm />}/>
          <Route path="/customers" element={<CustomerList />} />
          <Route path="/customers/create" element ={<CreateCustomerForm />}/>
          <Route path="/sales" element={<SaleList />} />
          <Route path="/sales/create" element ={<CreateSalesForm />}/>
          <Route path="/sales/history" element ={<SaleHistoryList />}/>
          <Route path="/automobiles" element={<AutomobileList />} />
          <Route path="/automobiles/create" element={<CreateAutomobileForm />} />
          <Route path="/manufacturers" element={<ManufacturerList />} />
          <Route path="/models" element={<ModelList />} />
          <Route path="/manufacturers/create" element={<ManufacturerForm />} />
          <Route path="/models/create" element={<VehicleModelForm />} />
          <Route path="/technicians/new" element={<TechnicianForm />} />
          <Route path="/technicians" element={<TechnicianList />} />
          <Route path="/appointments/history" element={<ServiceHistory />} />
          <Route path="/appointments/create" element={<AppointmentForm />} />
          <Route path="/appointments" element={<AppointmentList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
