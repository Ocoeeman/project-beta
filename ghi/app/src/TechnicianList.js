import React, { useEffect, useState } from 'react';

function TechnicianList() {
  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    async function loadTechnicians() {
        const technicianResponse = await fetch('http://localhost:8080/api/technicians/');
        if (technicianResponse.ok) {
          const technicianData = await technicianResponse.json();
          setTechnicians(technicianData.technicians);
        } else {
          console.log("Error fetching appointments data");
        }
      }
    loadTechnicians();
  }, []);

  return (
    <div className="px-4 py-5 my-5">
      <h1 className="display-5 fw-bold">Technicians</h1>
      <div className="mx-auto">
      <table className="table table-striped">
      <thead>
        <tr>
          <th>Employee ID</th>
          <th>First Name</th>
          <th>Last Name</th>
        </tr>
      </thead>
      <tbody>
        {technicians.map((technician) => {
          return (
            <tr key={technician.id}>
              <td>{technician.employee_id}</td>
              <td>{technician.first_name}</td>
              <td>{technician.last_name}</td>
            </tr>
          );
        })}
      </tbody>
    </table>

    </div>
    </div>
  );
}

export default TechnicianList;
