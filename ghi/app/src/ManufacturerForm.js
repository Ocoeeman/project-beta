import React, { useState } from 'react';

function ManufacturerForm() {
  const [formData, setFormData] = useState({
    name: '',
  });

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8100/api/manufacturers/';

    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        name: '',
      });
      alert("Manufacturer added successfully!");
    } else {
      alert("Failed to add manufacturer.");
    }
  };


  const handleFormChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevFormData) => ({ ...prevFormData, [name]: value }));
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a manufacturer</h1>
          <form onSubmit={handleSubmit} id="create-manufacturer-form">

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="employeeId">Manufacturer name...</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default ManufacturerForm;
