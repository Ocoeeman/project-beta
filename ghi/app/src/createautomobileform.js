import { useState, useEffect } from "react";

function CreateAutomobileForm(){
    const [color, setColor] = useState("");
    const [year, setYear] = useState("");
    const [vin, setVin] = useState("");
    const [model, setModel] = useState("");

    const handleColorChange = (banana) => {
        setColor(banana.target.value)
    }

    const handleYearChange = (coconut) => {
        setYear(coconut.target.value)
    }

    const handleVinChange = (pineapple) => {
        setVin(pineapple.target.value)
    }

    const handleModelChange = (pineapple) => {
        setModel(pineapple.target.value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;

        const Url = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(Url, fetchConfig);
        if (response.ok) {
            setColor('');
            setYear('');
            setVin('');
            setModel('')

        } else {
            console.error('Error sending form')
        }
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1 className="card-title">Add an Automobile</h1>
                <form onSubmit={handleSubmit} id="create-automobile-form">
                <div className="col">
                    <div className="form-floating mb-3">
                    <input required onChange={handleColorChange} value={color} placeholder="example Blue" type="text" id="color" name="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <input required onChange={handleYearChange} value={year} placeholder="example 2023" type="text" id="year" name="year" className="form-control" />
                    <label htmlFor="year">Year</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <input required onChange={handleVinChange} value={vin} placeholder="example 1C3CC5FB2AN120174" type="text" id="vin" name="vin" className="form-control" />
                    <label htmlFor="vin">VIN</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <input required onChange={handleModelChange} value={model} placeholder="example Sebring" type="text" id="model" name="model" className="form-control" />
                    <label htmlFor="model">Model</label>
                    </div>
                </div>
                <button className="btn btn-primary">Add Automobile</button>
                </form>
            </div>
        </div>
        </div>
    )

}

export default CreateAutomobileForm;
