import React, { useEffect, useState } from 'react';

function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [searchVIN, setSearchVIN] = useState('');

  useEffect(() => {
    async function loadAppointments() {
        const appointmentResponse = await fetch('http://localhost:8080/api/appointments/');
        if (appointmentResponse.ok) {
          const appointmentData = await appointmentResponse.json();
          setAppointments(appointmentData.appointments);
        } else {
          console.log("Error fetching appointments data");
        }
      }
    loadAppointments();
  }, []);

  const handleSearch = () => {
    const filteredAppointments = appointments.filter((appointment) =>
    appointment.vin ===searchVIN
    );
    setAppointments(filteredAppointments);
  };

  return (
    <div className="px-4 py-5 my-5">
      <h1 className="display-5 fw-bold">Service History</h1>
      <div className="row mb-4">
        <div className="col-md-9 col-lg-10">
          <input
            type="text"
            value={searchVIN}
            onChange={(e) => setSearchVIN(e.target.value)}
            className="form-control"
            placeholder="Search by VIN..."
          />
        </div>
        <div className="col-md-3 col-lg-2">
          <button className="btn btn-primary w-100" onClick={handleSearch}>Search</button>
        </div>
      </div>

      <div className="mx-auto">
      <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Customer</th>
          <th>Date</th>
          <th>Time</th>
          <th>Technician</th>
          <th>Reason</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        {appointments.map((appointment) => {
            const appointmentDate = new Date(appointment.date_time)
            const appointmentTime = new Date(appointment.date_time)

          return (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{appointment.customer}</td>
              <td>{appointmentDate.toLocaleDateString()}</td>
              <td>{appointmentTime.toLocaleTimeString()}</td>
              <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
              <td>{appointment.reason}</td>
              <td>{appointment.status}</td>
            </tr>
          );
        })}
      </tbody>
    </table>

    </div>
    </div>
  );
}

export default ServiceHistory;
