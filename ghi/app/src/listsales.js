import React, {useEffect, useState} from "react";

function SaleList() {
    const [sales, setSales] = useState([])

    async function loadSales() {
        const response = await fetch('http://localhost:8090/api/sales/')

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
        }
    }

    useEffect(() => {
        loadSales();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Automobile</th>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sales.map(auto => {
                    return (
                    <tr key={auto.id}>
                        <td>{auto.automobile.vin}</td>
                        <td>{auto.salesperson.first_name} {auto.salesperson.last_name}</td>
                        <td>{auto.customer.first_name} {auto.customer.last_name}</td>
                        <td>{auto.price}</td>
                    </tr>
                )})}
            </tbody>
        </table>
    )
}

export default SaleList
