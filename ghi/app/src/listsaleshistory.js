import React, { useEffect, useState } from "react";

function SaleList() {
  const [sales, setSales] = useState([]);
  const [filteredSales, setFilteredSales] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState("all");

  async function loadSales() {
    const response = await fetch('http://localhost:8090/api/sales/')

    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
      setFilteredSales(data.sales);
    }
  }

  useEffect(() => {
    loadSales();
  }, []);

  const handleDropDown = (event) => {
    const selectedSalespersonId = parseInt(event.target.value);

    if (selectedSalespersonId === "all") {
      setFilteredSales(sales);
    } else {
      const filteredSalesBySalesperson = sales.filter(
        (sale) => sale.salesperson.id === selectedSalespersonId
      );
      setFilteredSales(filteredSalesBySalesperson);
    }

    setSelectedSalesperson(selectedSalespersonId);
  };

  const getUniqueSalespersons = () => {
    const uniqueSalespersons = new Map();
    sales.forEach((sale) => {
      const { id, first_name, last_name } = sale.salesperson;
      uniqueSalespersons.set(id, { id, first_name, last_name });
    });
    return Array.from(uniqueSalespersons.values());
  };

  return (
    <div>
      <label>Select Salesperson:</label>
      <select onChange={handleDropDown} value={selectedSalesperson}>
        <option value="all">All Salespersons</option>
        {getUniqueSalespersons().map((salesperson) => (
          <option key={salesperson.id} value={salesperson.id}>
            {salesperson.first_name} {salesperson.last_name}
          </option>
        ))}
      </select>

      <table className="table table-striped">
        <thead>
          <tr>
            <th>Automobile</th>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {filteredSales.map((auto) => (
            <tr key={auto.id}>
              <td>{auto.automobile.vin}</td>
              <td>{auto.salesperson.first_name} {auto.salesperson.last_name}</td>
              <td>{auto.customer.first_name} {auto.customer.last_name}</td>
              <td>{auto.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default SaleList;
