import { useState, useEffect } from "react";

function CreateSalesForm(){
    const [salesperson, setSalesperson] = useState("");
    const [customer, setCustomer] = useState("");
    const [price, setPrice] = useState("");
    const [automobile, setAutomobile] = useState("");

    const handleSalespersonChange = (banana) => {
        setSalesperson(banana.target.value)
    }

    const handleCustomerChange = (coconut) => {
        setCustomer(coconut.target.value)
    }

    const handlePriceChange = (papaya) => {
        setPrice(papaya.target.value)
    }

    const handleAutomobileChange = (pineapple) => {
        setAutomobile(pineapple.target.value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.salesperson = salesperson;
        data.customer = customer;
        data.price = price;
        data.automobile = automobile;

        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            setSalesperson('');
            setCustomer('');
            setPrice('');
            setAutomobile('')

        } else {
            console.error('Error sending form')
        }
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1 className="card-title">Add a Sale</h1>
                <form onSubmit={handleSubmit} id="create-sales-form">
                <div className="col">
                    <div className="form-floating mb-3">
                    <input required onChange={handleSalespersonChange} value={salesperson} placeholder="example Mickey" type="text" id="salesperson" name="salesperson" className="form-control" />
                    <label htmlFor="salesperson">Salesperson</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <input required onChange={handleCustomerChange} value={customer} placeholder="example Mouse" type="text" id="customer" name="customer" className="form-control" />
                    <label htmlFor="customer">Customer</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <input required onChange={handlePriceChange} value={price} placeholder="example 1" type="text" id="price" name="price" className="form-control" />
                    <label htmlFor="price">Price</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <input required onChange={handleAutomobileChange} value={automobile} placeholder="example 1" type="text" id="automobile" name="automobile" className="form-control" />
                    <label htmlFor="address">Automobile</label>
                    </div>
                </div>
                <button className="btn btn-primary">Add Sales</button>
                </form>
            </div>
        </div>
        </div>
    )

}

export default CreateSalesForm;
